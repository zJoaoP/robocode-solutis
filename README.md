# Solutis Talent Sprint - Tars

Este repositório contém o código fonte do Tars, robô desenvolvido para competir na Solutis Robot Arena (Outubro, 2020).

## Estrutura Geral
Antes de escrever qualquer trecho de código, procurei entender qual o estado da arte e qual a estrutura de desenvolvimento frequentemente adotada pelos robôs mais competitivos existentes. Assim, o comportamento de Tars é representado pela junção dos seguintes módulos:

### Dados

O módulo de dados é o ponto central do Robô. Nele, estão armazenadas as informações mais atualizadas com respeito aos adversários, que são utilizadas a todo tempo pelos módulos de movimentação e artilharia. Cada robô adversário possui um objeto _EnemyData_ associado ao seu nome, que é atualizado a cada novo disparo do evento __onScannedRobot__.
```mermaid
graph TB
  subgraph "Tars Hierarchy"
  MovementNode(Movement Module) -- uses --> DataTrackerNode(DataTracker)
  TarsNode(Tars Behaviour)-- updates --> DataTrackerNode
  GunNode(Gun Module) -- uses --> DataTrackerNode
  MovementNode -- controls --> TarsNode
  GunNode -- controls --> TarsNode
end
```

### Movimentação

No robocode, a letalidade de um robô é altamente atrelada ao seu tempo de vida. Uma das técnicas mais interessantes de movimentação chama-se [Anti-Gravity Movement (AGM)](https://robowiki.net/wiki/Anti-Gravity_Movement), que cria _pontos de repulsão_ nas coordenadas dos adversários, induzindo o robô a permanecer sempre em regiões pouco povoadas do campo de batalha.

Esta é sem dúvidas uma técnica bastante eficiente, mas que pode induzir o robô a percorrer caminhos perigosos. Por conta destes riscos, surge [Minimum Risk Movement (MRM)](https://robowiki.net/wiki/Minimum_Risk_Movement), cuja ideia é "sortear" pontos seguindo algum critério e selecionar o ponto com o menor risco (dããã hahahahah).

```python
# Risk function. If the risk function is random, the behaviour is random. (e.g., Crazy)
def risk(point):
    return random.randint(0, 150)

def next_location(num_points):
    def generate_point():
        return (0, 0) # Some point generation method here.

    best = generate_sample()
    for _ in range(num_points):
        p = generate_point()
        if risk(best) > risk(s):
            best = s

    return best
```
Para Tars, uma boa localização é aquela que satisfaz aos seguintes critérios:

+ Não existe um adversário na linha entre Tars e a localização.
+ Ela maximiza a distância em relação ao adversário mais próximo.
+ Ela maximiza a distância em relação ao centro da arena.
+ Ela maximiza a distância em relação ao conjunto de **Pontos de Risco**.

A mescla entre o MRM e o AGM ocorre justamente nestes tais *Pontos de Risco*, que são justamente os pontos antigravidade descritos pelo AGM. Estes pontos são adicionados sempre que Tars sofre dano ou colide com um outro robô. Na prática, Tars une a busca pelas quinas do _Corners_ com a movimentação do _Walls_, mas de forma bem menos restritiva.

### Artilharia

Para este módulo, foi considerado que a maior parte dos robôs mantém sua trajetória em linha reta. Assim, Tars assume que o adversário terá direção e velocidade constantes nas próximas $`t`$ unidades de tempo (1) e dispara no ponto futuro, técnica conhecida como [Linear Targeting](https://robowiki.net/wiki/Linear_Targeting).

Para descobrir o valor de $`t`$ (ou mesmo se ele existe), é necessário primeiro definir o momento do impacto:

```math
tars.getX() + \sin(gHeading) * t * bSpeed = e.getX() + \sin(eHeading) * t * eSpeed
```
```math
tars.getY() + \cos(gHeading) * t * bSpeed = e.getY() + \cos(eHeading) * t * eSpeed
```
Isolando senos e cossenos acima, temos:

```math
\sin(gHeading) = \dfrac{e.getX() - tars.getX()}{t * bSpeed} + \dfrac{\sin(eHeading) * eSpeed)}{bSpeed}
```
```math
\cos(gHeading) = \dfrac{e.getY() - tars.getY()}{t * bSpeed} + \dfrac{\cos(eHeading) * eSpeed)}{bSpeed}
```
Para reduzir o trabalho nos cálculos, é possível reduzir estas equações a:

```math
\sin(gHeading) = \dfrac{A}{t} + B
```
```math
\cos(gHeading) = \dfrac{C}{t} + D
```

Enfim, aplicado a identidade trigonométrica $`\sin^2\theta + \cos^2\theta = 1`$, temos:

```math
\bigg(\dfrac{A}{t} + B\bigg)^2 + \bigg(\dfrac{C}{t} + D\bigg)^2 = 1 \therefore (A^2 + C^2) * \bigg(\dfrac{1}{t}\bigg)^2 + 2(AB + CD) * \bigg(\dfrac{1}{t}\bigg) + (B^2 + D^2 - 1) = 0
```

que está na forma quadrática. Sabendo o valor de $`t`$ e a potência pretendida para o disparo, é possível determinar o ponto de impacto e, assim, o ângulo de ajuste do canhão. Existem soluções aproximadas mais curtas para este problema (frequente utilizadas em robôs Nano), mas estas são geralmente imprecisas (no caso das aproximadas) ou possuem custo computacional elevado (no caso das iterativas), o que pode levar à perda de turnos e atrasos no processamento.

## Quais os pontos fortes?
Agora, sabendo das principais características do comportamento de Tars, podemos enfim elaborar a respeido de seus pontos mais fortes:

+ Tars é um robô com boa capacidade de adaptação e movimentação, muito pela sua capacidade de manter-se afastado do centro da arena e de outros competidores sem necessariamente manter-se grudado nas bordas.

+ A artilharia de Tars possui excelente precisão contra [Rammers](https://robowiki.net/wiki/Ramming_Movement) e contra robôs de movimento constante (1).
<div align="center">
    <img src="assets/scoreboard.png">
    <figcaption>Fig.1 - Performance de Tars contra robôs mais previsíveis</figcaption>
</div>

## E sobre os pontos fracos?

+ Quando os adversários não se movimentam de forma constante, Tars pode encontrar dificuldade para encaixar bons disparos. Por conta disto, sua performance cai contra robôs de movimentação aleatória ([Crazy](https://robowiki.net/wiki/Crazy)) e circular ([SpinBot](https://robowiki.net/wiki/SpinBot)).

+ Em certos rounds, Tars pode não conseguir escapar de uma localização pouco vantajosa. É comum vê-lo encurralado em quinas ou agarrado em Rammers.

+ Tars tenta alvejar apenas o seu alvo mais próximo e não muda de alvo caso seja atingido por um alvo bem mais distante. É possível que Tars tente eliminar um alvo mais próximo (mas que consegue esquivar de todos os disparos, como Crazy e SpinBot) enquanto é alvejado brutalmente por um outro robô.

## No fim das contas, o que aprendi?

+ Foi muito interessante e desafiador voltar a desenvolver em Java (mesmo sem tanta experiência prévia, contando com pequenas implementações em disciplinas de Engenharia de Software), principalmente por ter sido meu primeiro contato com o Robocode e com sua documentação.

+ A natureza discreta e parcialmente observável do ambiente dá margem para diversas implementações, algumas com performance quase perfeita (como [Diamond](https://robowiki.net/wiki/Diamond)) e outras precisamente imperfeitas (como [Awful](https://robowiki.net/wiki/Awful)).