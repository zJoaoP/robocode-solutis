package solutis.gun;

import robocode.RoundEndedEvent;
import robocode.ScannedRobotEvent;
import solutis.Tars;

public abstract class GunStrategy {
	protected Tars robot;
	
	public GunStrategy(Tars robot) {
		this.robot = robot;
	}
	
	public abstract void onScannedRobot(ScannedRobotEvent e);
	
	public void onRoundEnded(RoundEndedEvent e) {
		
	}
}
