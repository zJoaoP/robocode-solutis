package solutis.gun;

import robocode.Rules;
import robocode.ScannedRobotEvent;
import robocode.util.Utils;
import solutis.Tars;
import solutis.core.DataTracker;

public class LinearTargetingGun extends GunStrategy{
	static final int WALL_LIMIT = 20;
	
	public LinearTargetingGun(Tars robot) {
		super(robot);
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		DataTracker tracker = robot.getTracker();
		
		if(!tracker.isNearestEnemy(e.getName()))
			return;
		
		double absoluteBearing = robot.getHeadingRadians() + e.getBearingRadians();
		double enemyX = robot.getX() + Math.sin(absoluteBearing) * e.getDistance();
		double enemyY = robot.getY() + Math.cos(absoluteBearing) * e.getDistance();
		double bulletPower = Math.min(2.5, robot.getEnergy() / 10.0);
		double bulletSpeed = Rules.getBulletSpeed(bulletPower);
		
		double B = e.getVelocity() / bulletSpeed * Math.sin(e.getHeadingRadians());
		double D = e.getVelocity() / bulletSpeed * Math.cos(e.getHeadingRadians());
		double A = (enemyX - robot.getX()) / bulletSpeed;
		double C = (enemyY - robot.getY()) / bulletSpeed;
		
		double a = (A * A + C * C);
		double b = 2 * (A * B + C * D);
		double c = (B * B) + (D * D) - 1.0;
		
		double delta = b * b - 4 * a * c;
		if(delta >= 0) {
			double t1 = (2 * a) / (-b + Math.sqrt(delta));
			double t2 = (2 * a) / (-b - Math.sqrt(delta));
			
			double t = (Math.min(t1, t2) >= 0) ? Math.min(t1, t2): Math.max(t1, t2);
			double contactX = enemyX + Math.sin(e.getHeadingRadians()) * t * e.getVelocity();
			double contactY = enemyY + Math.cos(e.getHeadingRadians()) * t * e.getVelocity();
			
			contactX = fitInRange(contactX, WALL_LIMIT, robot.getBattleFieldWidth() - WALL_LIMIT);
			contactY = fitInRange(contactY, WALL_LIMIT, robot.getBattleFieldHeight() - WALL_LIMIT);
			
			double angle = Math.atan2(contactX - robot.getX(), contactY - robot.getY());
			double offset = Utils.normalRelativeAngle(angle - robot.getGunHeadingRadians());
			
			robot.turnGunRightRadians(offset);
			if(robot.getGunHeat() == 0 && robot.setFireBullet(bulletPower) != null) {
				
			}
		}
	}
	
	public double fitInRange(double value, double min, double max) {
		return Math.min(max, Math.max(min, value));
	}
}
