package solutis.movement;

import java.awt.Graphics2D;
import java.awt.geom.Point2D;

import robocode.RoundEndedEvent;
import solutis.Tars;

public abstract class MovementStrategy {	
	protected Tars robot;
	
	public MovementStrategy(Tars robot) {
		this.robot = robot;
	}
	
	public void drawPoint(Point2D p, Graphics2D g) {
		g.fillOval((int) p.getX(), (int) p.getY(), 5, 5);
	}
	
	public void onRoundEnded(RoundEndedEvent e) {
		
	}
	
	public abstract Point2D getDestination();
	public abstract void onPaint(Graphics2D g);
}
